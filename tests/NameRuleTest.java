package tests;

import Checker.CheckResult;
import Checker.NameRule;
import Checker.RuleResult;
import domain.Person;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class NameRuleTest {

    private NameRule rule;

    @Before
    public void setUp() {
        this.rule = new NameRule();
    }
    @Test
    public void testIfNameIsNullThrowsException() {
        Person person = new Person();

        RuleResult actual = this.rule.checkRule(person).getResult(),
                excepted = RuleResult.Exception;

        assertEquals(actual, excepted);
    }
    @Test
    public void testIfNameIsEmptyReturnsError() {
        Person person = new Person();
        person.setFirstName("");
        RuleResult actual = this.rule.checkRule(person).getResult(),
                excepted = RuleResult.Error;

        assertEquals(actual, excepted);
    }
    @Test
    public void testIfProperlyNameIsOk() {
        Person person = new Person();
        person.setFirstName("Pafcio");
        RuleResult actual = this.rule.checkRule(person).getResult(),
                excepted = RuleResult.Ok;

        assertEquals(actual, excepted);
    }
}