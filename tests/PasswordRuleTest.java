package tests;

import
        Checker.CheckRule;
import Checker.PasswordRule;
import Checker.RuleResult;
import domain.User;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class PasswordRuleTest {

    private CheckRule rule;
    @Before
    public void setUp() throws Exception {
        this.rule = new PasswordRule();
    }

    @Test
    public void testIfPasswordIsNullThrowsException() {
        User user = new User();

        RuleResult actual = this.rule.checkRule(user).getResult(),
                excepted = RuleResult.Exception;

        Assert.assertEquals(actual, excepted);
    }
    @Test
    public void testIfPasswordIsEmptyReturnsError() {
        User user = new User();
        user.setPassword("");
        RuleResult actual = this.rule.checkRule(user).getResult(),
                excepted = RuleResult.Error;

        Assert.assertEquals(actual, excepted);
    }
    @Test
    public void testIfPasswordIsNotSevenCharacterLengthReturnError() {
        User user = new User();
        user.setPassword("1234");
        RuleResult actual = this.rule.checkRule(user).getResult(),
                excepted = RuleResult.Error;

        Assert.assertEquals(actual, excepted);
    }
    @Test
    public void testIfPasswordIsSevenCharacterLengthReturnError() {
        User user = new User();
        user.setPassword("123456789");
        RuleResult actual = this.rule.checkRule(user).getResult(),
                excepted = RuleResult.Ok;

        Assert.assertEquals(actual, excepted);
    }
}