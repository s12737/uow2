package tests;

import Checker.NameRule;
import Checker.PeselRule;
import Checker.RuleResult;
import domain.Person;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class PeselRuleTest {
    private PeselRule rule;

    @Before
    public void setUp() {
        this.rule = new PeselRule();
    }

    @Test
    public void testIfPeselIsNullThrowsException() {
        Person person = new Person();

        RuleResult actual = this.rule.checkRule(person).getResult(),
                excepted = RuleResult.Exception;

        assertEquals(actual, excepted);
    }
    @Test
    public void testIfPeselIsEmptyReturnsError() {
        Person person = new Person();
        person.setPesel("");
        RuleResult actual = this.rule.checkRule(person).getResult(),
                excepted = RuleResult.Error;

        assertEquals(actual, excepted);
    }
    @Test
    public void testIfProperlyPeselIsOk() {
        Person person = new Person();
        person.setPesel("901010405012");
        RuleResult actual = this.rule.checkRule(person).getResult(),
                excepted = RuleResult.Ok;

        assertEquals(actual, excepted);
    }
}