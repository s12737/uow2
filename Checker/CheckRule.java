package Checker;

public interface CheckRule<TEntity> {
    CheckResult checkRule(TEntity entity);
}
