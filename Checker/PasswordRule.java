package Checker;

import domain.User;

public class PasswordRule implements CheckRule {
    @Override
    public CheckResult checkRule(Object o) {
        CheckResult result = new CheckResult();

        if( ((User)o).getPassword() == null)
            result.setResult(RuleResult.Exception);
        else if(((User) o).getPassword() == "" || ((User) o).getPassword().length() < 7)
            result.setResult(RuleResult.Error);
        else
            result.setResult(RuleResult.Ok);

        return result;
    }
}
